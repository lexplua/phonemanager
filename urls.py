from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic import TemplateView
from django.contrib import admin
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.base import RedirectView

admin.autodiscover()

urlpatterns = patterns('',

    url(r'^$', login_required(TemplateView.as_view(template_name='base.html'))),
    url(r'^accounts/login/$','django.contrib.auth.views.login',{'template_name': 'login.html'},name='login'),
    url(r'^accounts/logout/','django.contrib.auth.views.logout',{'next_page': '/',},name='logout'),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^manager/', include('accounting.urls')),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT,
        'show_indexes': True}),
    url(r'^favicon\.ico$', RedirectView.as_view(url='/static/favicon.ico')),
)
urlpatterns += patterns('django.contrib.staticfiles.views',url(r'^static/(?P<path>.*)$', 'serve'),)

urlpatterns+=staticfiles_urlpatterns()
