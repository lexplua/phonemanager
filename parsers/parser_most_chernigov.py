 # -*- coding: utf-8 -*-
from xlrd import open_workbook
class Parser:
    def __call__(self, filename, **kwargs):
        res = []
        wb = open_workbook(filename)
        for row in range(wb.sheets()[0].nrows)[3:]:
            values = []
            for col in range(wb.sheets()[0].ncols):
                if not col:
                    values.append(unicode(wb.sheets()[0].cell(row,col).value)[:-2])
                if col in [1,2]:
                    values.append(unicode(wb.sheets()[0].cell(row,col).value))
                if col == 3:
                    values.append('380'+ str(wb.sheets()[0].cell(row,col).value)[:-2])
            res.append([x.strip() for x in values])
        for i in res:
             print ' | '.join(i)
        return res

#a = Parser()(u'Мост Чернигов.xls')

