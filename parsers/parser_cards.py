# -*- coding: utf-8 -*-
from xlrd import open_workbook
from datetime import date, datetime, timedelta
import xlrd
class Parser:
    def __call__(self, filename, **kwargs):
        wb = open_workbook(filename)
        res = []
        town = ""
        for sheetnum in range(len(wb.sheets())):
            for row in range(wb.sheets()[sheetnum].nrows)[8:]:
                values = []
                str = wb.sheets()[sheetnum].cell(row,5).value
                if isinstance(str,unicode) and str.strip().startswith(u"Явир"):
                    town = unicode(' '.join(str.strip().split(' ')[1:]))
                    continue
                for col in range(wb.sheets()[sheetnum].ncols):
                    if col == 5 and wb.sheets()[0].cell(row,col).value:
                        str = wb.sheets()[sheetnum].cell(row,5).value
                        if isinstance(str,unicode) and str.strip().startswith(u"Карточка"):
                            values.append(str.strip().split(' ')[2].replace('-',''))
                if town!=u'':
                    values.append(town)
                if len(values)>1:
                    res.append(values)
            return res
#a = Parser()(u'/home/nastya/PhoneManager/docs/Карточки.xls')
#for i in a:
#    print ' | '.join([unicode(x) for x in i])
