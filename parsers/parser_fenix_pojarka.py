 # -*- coding: utf-8 -*-
from xlrd import open_workbook
class Parser:
    def __call__(self, filename, **kwargs):
        wb = open_workbook(filename)
        res = []
        for row in range(wb.sheets()[0].nrows)[2:]:
            values = []
            for col in range(wb.sheets()[0].ncols):
                if col == 0:
                    values.append(unicode(wb.sheets()[0].cell(row,col).value.strip())[1:])
                if col in [1,2,6]:
                    values.append(wb.sheets()[0].cell(row,col).value.strip())
            res.append([x.strip() for x in values])
        return res

#a = Parser()(u'Феникс пожарка.xls')
#for i in a:
#     print ' | '.join(i)