# -*- coding: utf-8 -*-
__author__ = 'lex'
import re,codecs
class Parser:
    def __call__(self, filename, **kwargs):
        f = codecs.open(filename,mode='r',encoding='cp1251')
        result = []
        lines = [x.strip() for x in f.readlines() if x.strip()][2:]
        line_iter = iter(lines)
        #regexp = ur'^^(?P<id_c>\d+\.\d+\.\d+)\s+\w+\s*\d+[/|\.]+\d+\s+[Да|Нет|Плохая]+\s+[Да|Нет|Плохая]+\s+[Есть|Нет|Плохая]+\s+(?![Да|Нет|Плохая|Есть]+)(?P<address>\w[\w\s{1:3}\.,"/\\\(\)-]+)'
        regexp = ur'^^(?P<id_c>\d+\.\d+\.\d+).*[Да|Нет]+\s+[Да|Нет]+\s+[Есть|Нет|Плохая]+\s+(?![Да|Нет|Плохая|Есть]+)\s+(?P<address>\w[\w\s\.,"/\\\(\)-]+[\w\.,"/\\\(\)-])'
        exp = re.compile(regexp,re.U)
        phone_exp1 = re.compile(r'\+(?P<phone>[[\d]{12}|на \w+\-\d+])',re.U)
        phone_exp2 = re.compile(r'\s(?P<phone>\w+\-\d+)',re.U)
        while 1:
            try:
                res = None
                id_c,address,phone = "",'',''
                i = line_iter.next()
                res = re.search(exp,i)
                if res:
                    id_c,address= res.groups()
                    if "      " in address:
                        address = re.split(r'\s{5}',address)[0].strip()
                    res = re.search(phone_exp1,i)
                    if res:
                        phone, = res.groups()
                    else:
                        res = re.search(phone_exp2,i)
                        if res:
                            phone, = res.groups()
                        else:raise ValueError

                result.append([x.strip() for x in (id_c,address,address,phone)])
                #print id_c,"",address,'>>>',phone
            except StopIteration:
                break
            except IndexError:
                pass
            except ValueError,e:
                pass
        return result
#p = Parser()
#p(u"/home/lex/projects/Javir2000/PhoneManager/docs/Дунай.txt")
