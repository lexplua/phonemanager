 # -*- coding: utf-8 -*-
__author__ = 'lex'
import re,codecs

class Parser:
    def __call__(self, filename, **kwargs):
        f = codecs.open(filename,mode='r',encoding='cp1251')
        lines = [x.strip() for x in f.readlines() if x.strip()][2:]
        result = []
        for i in xrange(len(lines)):
            try:
                line = lines[i]
                splitted_line = line.split(' ')
                pointer = 1
                while not splitted_line[pointer].isdigit():
                    pointer+=1
                id_c = splitted_line[pointer]
                splitted_line = ' '.join(splitted_line[pointer+1:]).strip()
                name_obj,address = [x.strip() for x in re.split(r'\s{4,50}',splitted_line) if x.strip()][0:3]
                i+=2
                line = lines[i]
                phone = line.strip()
                if not phone.isdigit()  and not phone.startswith(u"на "):
                    i-=1
                    raise ValueError
                phone = "3"+phone
                result.append([x.strip() for x in [id_c,name_obj,address,phone]])
                print id_c,name_obj,address,phone
            except StopIteration:
                break
            except IndexError:
                pass
            except ValueError:
                pass
        return result
#p = Parser()
#p(u"/home/lex/projects/Javir2000/PhoneManager/docs/Грифон.txt")