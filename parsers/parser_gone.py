# -*- coding: utf-8 -*-
from xlrd import open_workbook
from datetime import date, datetime, timedelta
import xlrd
class Parser:
    def __call__(self, filename, **kwargs):
        wb = open_workbook(filename)
        res = []
        town = ""
        for sheetnum in range(len(wb.sheets())):
            for row in range(wb.sheets()[sheetnum].nrows)[1:]:
                values = []
                if unicode(wb.sheets()[sheetnum].cell(row,0).value).startswith(u"Ушедш"):
                    town = unicode(' '.join(wb.sheets()[sheetnum].cell(row,0).value.split(' ')[2:]))
                    continue
                if unicode(wb.sheets()[sheetnum].cell(row,1).value).startswith(u"Ушедш"):
                    town = unicode(' '.join(wb.sheets()[sheetnum].cell(row,1).value.split(' ')[2:]))
                    continue
                if unicode(wb.sheets()[sheetnum].cell(row,1).value).startswith(u"Название"):
                    continue
                for col in range(wb.sheets()[sheetnum].ncols):
                    if col in [1,6,7] and wb.sheets()[sheetnum].cell(row,col).value:
                        if isinstance(wb.sheets()[sheetnum].cell(row,col).value,float):
                            values.append((int(wb.sheets()[sheetnum].cell(row,col).value)))
                        else:
                            values.append((wb.sheets()[sheetnum].cell(row,col).value))
                    if col == 2 and wb.sheets()[sheetnum].cell(row,col).value:
                        values.append(town + ' ' + unicode(wb.sheets()[sheetnum].cell(row,col).value))
                    if (col == 3 or col == 8) and str(wb.sheets()[sheetnum].cell(row,col).value)!=u'':
                        try:
                            values.append(date(*xlrd.xldate_as_tuple(wb.sheets()[sheetnum].cell(row,col).value, wb.datemode)[:3]))
                        except :
                            print 'Uncorrect date in row: ' + str(row) + ', col: ' + str(col)
                            raise
                if values:
                    res.append([(x.strip() if isinstance(x,unicode) else x) for x in values])
        return res

if __name__ == '__main__':
    a = Parser()(u'/home/nastya/PhoneManager/docs/Ушедшие 2012.xls')
    for i in a:
        print ' | '.join([unicode(x) for x in i])
