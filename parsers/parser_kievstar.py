 # -*- coding: utf-8 -*-
from xlrd import open_workbook
from datetime import datetime
class Parser:
    def __call__(self, filename, **kwargs):
        wb = open_workbook(filename)
        res = []
        result = {
            u'Январь': '01',
            u'Февраль': '02',
            u'Март': '03',
            u'Апрель': '04',
            u'Май': '05',
            u'Июнь': '06',
            u'Июль': '07',
            u'Август': '08',
            u'Сентябрь': '09',
            u'Октябрь': '10',
            u'Ноябрь': '11',
            u'Декабрь': '12',
        }[unicode(wb.sheets()[0].cell(0,0).value.split(' ')[6])]
        date = datetime.strptime(unicode('01 ' + result + " " + wb.sheets()[0].cell(0,0).value.split(' ')[7]),'%d %m %Y')
        for row in range(wb.sheets()[0].nrows)[2:]:
            values = []
            for col in range(wb.sheets()[0].ncols):
                if col == 4:
                    values.append("380"+unicode(wb.sheets()[0].cell(row,col).value).strip())
                    values.append(date)
                if col == 6:
                    values.append(unicode(str(wb.sheets()[0].cell(row,col).value).strip().replace(',','.')))
            res.append(values)
        return res

#p = Parser()
#print p(u'/home/nastya/PhoneManager/docs/Счет.xls')


