# -*- coding: utf-8 -*-
from xlrd import open_workbook
class Parser:
    def __call__(self, filename, **kwargs):
        wb = open_workbook(filename)
        res = []
        for row in range(wb.sheets()[0].nrows)[3:]:
            values = []
            for col in range(wb.sheets()[0].ncols):
                if col in [1,3] and wb.sheets()[0].cell(row,col).value:
                    values.append(wb.sheets()[0].cell(row,col).value)
            if(len(values)>1):
                res.append([x.strip() for x in values])
        return res
if __name__ == '__main__':
    a = Parser()(u'/home/nastya/PhoneManager/docs/Номера Пультов.xls')
    for i in a:
        print ' | '.join(i)
