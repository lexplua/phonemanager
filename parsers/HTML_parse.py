# -*- coding: utf-8 -*-
import codecs
import lxml.html
from datetime import datetime
class Parser():
    def __call__(self, filename, **kwargs):
        f = codecs.open(filename,mode='r',encoding='cp1251')
        lines = f.read()
        doc = lxml.html.document_fromstring(lines)
        date =''
        res = []
        for i in doc.cssselect('b'):
            if i.text.startswith(u"РОЗРАХУНКОВИЙ"):
                date_str = i.text[-23:]
                date = datetime.strptime(date_str.split('-')[0].strip(), '%d.%m.%Y')
                #print date
        num = 0
        val = []
        selectorResult = doc.cssselect('b')
        for i in range(len(selectorResult)):
            if selectorResult[i].text.startswith(u"Номер телефону") and num == 0:
                val.append("380"+selectorResult[i].text.split(':')[1].strip())
                num = 1
            if num == 1:
                val.append(date)
                num = 2
            if selectorResult[i].text.startswith(u"Загалом за контрактом:") and num == 2:
                val.append(selectorResult[i+1].text.strip())
                num = 0
                res.append(val)
                val = []
        return res
#p = Parser()
#rint p(u'/home/nastya/Tel_Project/MTS_invoice_5.28000.00.00.100000_2012_06.htm')





