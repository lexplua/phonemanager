 # -*- coding: utf-8 -*-
from xlrd import open_workbook
class Parser:
    def __call__(self, filename, **kwargs):
        wb = open_workbook(filename)
        res = []
        for row in range(wb.sheets()[0].nrows)[3:]:
            values = []
            for col in range(wb.sheets()[0].ncols):
                if col in [0,1,2,6]:
                    values.append(wb.sheets()[0].cell(row,col).value)
            res.append([x.strip() for x in values])
        return res

#a = Parser()(u'ФЕНИКС.xls')
#for i in a:
#    print ' | '.join(i)
