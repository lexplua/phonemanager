__author__ = 'lex'
from django.contrib import admin
from accounting.models import *


class ContractTab(admin.TabularInline):
    model = Contract
    list_display = ('number', 'date', 'contracted', 'card')
    extra = 0


class ObjectTab(admin.TabularInline):
    model = Object.cards.through
    list_display = ('name', 'hub', 'address', 'branch','not_alive','delete_date')
    extra = 0


class ObjectTabForBranch(admin.TabularInline):
    model = Object
    list_display = ('name', 'hub', 'address', 'branch','not_alive','delete_date')
    extra = 0

class BillsAdmin(admin.ModelAdmin):
    list_display = ('card', 'date', 'cost')
    list_filter = ('card', 'date', 'cost')


class CardAdmin(admin.ModelAdmin):
    list_display = ('number', 'state', 'new_state', 'P_accepted',
                    'OD_accepted', 'B_accepted', 'branch')
    list_filter = ('number', 'state', 'new_state', 'P_accepted',
                   'OD_accepted', 'B_accepted', 'branch')
    inlines = (
        ObjectTab,
        )
    search_fields = ('number',)

class HubAdmin(admin.ModelAdmin):
    list_display = ('number',)
    inlines = (
        ContractTab,
        )


class BranchAdmin(admin.ModelAdmin):
    list_display = ('name',)
    inlines = (
        ObjectTabForBranch,
        )


class ObjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'hub', 'address', 'branch')
    list_filter = ('name', 'hub', 'address', 'branch')


class ContractAdmin(admin.ModelAdmin):
    list_display = ('number', 'date', 'contracted', 'hub')
    list_filter = ('number', 'date', 'contracted', 'hub')


class IncomeObjectAdmin(admin.ModelAdmin):
    list_display = ('object',)


class OutcomeObjectAdmin(admin.ModelAdmin):
    list_display = ('object',)


admin.site.register(Card, CardAdmin)
admin.site.register(Hub, HubAdmin)
admin.site.register(Branch, BranchAdmin)
admin.site.register(Object, ObjectAdmin)
admin.site.register(Bills, BillsAdmin)
admin.site.register(Contract, ContractAdmin)
admin.site.register(IncomeObject, IncomeObjectAdmin)
admin.site.register(OutcomeObject, OutcomeObjectAdmin)