__author__ = 'lex'
from django import template
register = template.Library()

@register.filter
def get_bill_by_month(instance, args):
    """
    allows only one argument for one exact method
    """
    return instance.get_bill_by_month(
        [int(x) for x in args.split(';')][0],
        [int(x) for x in args.split(';')][1]
    )
