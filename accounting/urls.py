from django.conf.urls import patterns, url
from django.views.generic import RedirectView
import accounting.views
urlpatterns = patterns('',
    url(r'^addpultdata/$', accounting.views.UploadControlData.
    as_view(), {}, 'upload-new-pultdata'),
    url(r'^addbilldata/$', accounting.views.UploadBillData.as_view(
    ), {}, 'upload-new-billdata'),
    url(r'^addincomedata/$', accounting.views.UploadIncomeData.
    as_view(), {}, 'upload-new-incomedata'),
    url(r'^addoutcomedata/$', accounting.views.UploadOutcomeData.
    as_view(), {}, 'upload-new-outcomedata'),
    url(r'^cards_list/page(?P<page>\d+)/(?P<month>\d+)/(?P<year>\d+)/',
        accounting.views.CardsView.as_view(), {}, 'cards-list'),
    url(r'^export/(?P<month>\d+)/(?P<year>\d+)/', accounting.views.
    ExportXLS.as_view(), {}, 'export'),
    url(r'^setfilter/(?P<filter_type>\w+)/(?P<filter_parameter>[^/]+)/$',
        accounting.views.EnableFilterView.as_view(), {}, 'set-filter'),
    url(r'^cards_list-income/', RedirectView.as_view(
        url='/admin/accounting/incomeobject'), {}, 'cards-list-income'),
    url(r'^cards_list-outcome/', RedirectView.as_view(
        url='/admin/accounting/outcomeobject'), {}, 'cards-list-outcome'),
)
