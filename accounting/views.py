# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.edit import FormView
from django.views.generic import ListView, View
from accounting.forms import UploadFileForm
from accounting.models import *
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import simplejson
from django.db.transaction import commit_on_success
from django.conf import settings
from django.core.urlresolvers import reverse
from os import path
import xlwt


def response_mimetype(request):
    if "application/json" in request.META['HTTP_ACCEPT']:
        return "application/json"
    else:
        return "text/plain"


class JSONResponse(HttpResponse):
    """JSON response class."""
    def __init__(self, obj='', json_opts={}, mimetype="application/json", *args, **kwargs):
        content = simplejson.dumps(obj, **json_opts)
        super(JSONResponse, self).__init__(content, mimetype, *args, **kwargs)


class UploadControlData(FormView):
    form_class = UploadFileForm
    success_url = '/'
    template_name = 'uploadformpult.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return  super(UploadControlData, self).dispatch(request, *args, **kwargs)

    @commit_on_success
    def form_valid(self, form):
        in_file = self.request.FILES['file']
        filename = in_file.name
        parser = None
        if filename == u'Дунай.txt':
            from parsers import parser_Dunai
            parser = parser_Dunai.Parser()
        if filename == u'Грифон.txt':
            from parsers import parser_Grifon
            parser = parser_Grifon.Parser()
        if filename == u'ФЕНИКС.xls':
            from parsers import parser_fenix
            parser = parser_fenix.Parser()
        if filename == u'Феникс пожарка.xls':
            from parsers import parser_fenix_pojarka
            parser = parser_fenix_pojarka.Parser()
        if filename == u'Мост Чернигов.xls':
            from parsers import parser_most_chernigov
            parser = parser_most_chernigov.Parser()
        filepath = self.request.FILES['file'].temporary_file_path()
        data = parser(filepath)
        for item in data:
            hub = Hub.objects.get_or_create(pk=item[0])[0]
            hub.save()
            card, created = Card.objects.get_or_create(
                pk=item[3],
                defaults={
                    'state': 'W',
                    'new_state': 'W',
                    'P_accepted': True,
                    'OD_accepted': True,
                    'B_accepted': True,
                }
            )
            if not item[3]:continue
            card.save()
            _object, created = Object.objects.get_or_create(
                hub=hub,
                name = item[1],
                address = item[2],
            )
            _object.cards.add(card,)
            _object.save()
        data = [{u'name': filename, u'success': True}]
        response = JSONResponse(data, {}, response_mimetype(self.request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response


class UploadBillData(FormView):
    form_class = UploadFileForm
    success_url = '/'
    template_name = 'uploadformbill.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return  super(UploadBillData, self).dispatch(request, *args, **kwargs)

    @commit_on_success
    def form_valid(self, form):
        in_file = self.request.FILES['file']
        filename = in_file.name
        parser = None
        if  'xls' in filename:
            from parsers import parser_kievstar
            parser = parser_kievstar.Parser()
        if 'htm' in filename:
            from parsers import HTML_parse
            parser = HTML_parse.Parser()
        filepath = self.request.FILES['file'].temporary_file_path()
        data = parser(filepath)
        for item in data:
            card, created = Card.objects.get_or_create(
                pk=item[0],
                defaults={
                    'state': 'W',
                    'new_state': 'W',
                    'P_accepted': True,
                    'OD_accepted': True,
                    'B_accepted': True,
                }
            )
            if created:
                card.save()
            _bills, created = Bills.objects.get_or_create(
                card=card,
                date=item[1],
                defaults={
                    'cost': item[2]
                }

            )
            _bills.save()
        data = [{u'name': filename, u'success': True}]
        response = JSONResponse(data, {}, response_mimetype(self.request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response


class CardsView(ListView):
    model = Card
    template_name = 'cards.html'
    context_object_name = 'cards'
    paginate_by = 20
    allow_empty = True

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        self.year = kwargs['year']
        self.month = kwargs['month']
        if not request.META.get('HTTP_REFERER', False):
            request.session['filter'] = {}
            request.session['filter']['type'] = 'none'
        return  super(CardsView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        if not self.request.session.get('filter', False) or self.request.session['filter']['type'] == 'none':
            #TODO:// Bad and slow query, replace this shet
            #queryset = Card.objects.extra(select={'cost': "select accounting_bills.cost from accounting_bills  where accounting_bills.date = '{0}-{1}-01' and accounting_bills.card_id in (select accounting_object_cards.card_id from accounting_object_cards where accounting_object_cards.object_id=accounting_card.number)".format(str(self.year), str(self.month))})
            #return  [x for x in queryset if x.cost]
            bills = Bills.objects.filter(
                Q(date__month=str(self.month)) &
                Q(date__year=str(self.year))
            )#.prefetch_related('card')
            return [x.card for x in bills]
        if self.request.session['filter']['type'] == 'order':
            if self.request.session['filter']['parameter'] == 'minmax':
                #TODO:// Bad and slow query, replace this shet
                #queryset = Card.objects.extra(
                #    select={'cost': "select accounting_bills.cost from accounting_bills  where accounting_bills.date = '{0}-{1}-01' and accounting_card.number = accounting_bills.card_id".format(str(self.year), str(self.month))}
                #).order_by('cost')
                #return  [x for x in queryset if x.cost]
                bills = Bills.objects.filter(
                    Q(date__month=str(self.month)) &
                    Q(date__year=str(self.year))
                ).order_by('cost')
                return  [x for x in bills if x.cost]
            else:
                #TODO:// Bad and slow query, replace this shet
                #queryset = Card.objects.extra(select={'cost': "select accounting_bills.cost from accounting_bills  where accounting_bills.date = '{0}-{1}-01' and accounting_card.number = accounting_bills.card_id".format(str(self.year), str(self.month))}).order_by('-cost')
                #return  [x for x in queryset if x.cost]
                bills = Bills.objects.filter(
                    Q(date__month=str(self.month)) &
                    Q(date__year=str(self.year))
                ).order_by('-cost')
                return  [x for x in bills if x.cost]
        if self.request.session['filter']['type'] == 'filial':
            branch = Branch.objects.filter(name__startswith=self.
                                                            request.session['filter']['parameter'])
            return ([x for x in branch[0].card_set.all()] if len(branch) else [])

        if self.request.session['filter']['type'] == 'limit':
            bills = Bills.objects.filter(
                Q(date__month=str(self.month)) &
                Q(date__year=str(self.year)) &
                Q(cost__gt=int(self.request.session['filter']['parameter']))
            ).prefetch_related('card')
            return [x.card for x in bills]

        if self.request.session['filter']['type'] == 'state':
            return  Card.objects.filter(Q(state__exact='F')|Q(state__exact='R'))

    def get_context_data(self, **kwargs):

        context = super(CardsView, self).get_context_data(**kwargs)
        context['years'] = [str(x) for x in range(2012, 2012 + 61)]
        context['year'] = str(self.year)
        try:
            if self.request.session['filter']['type'] == 'limit':
                context['limit'] = self.request.session[
                                   'filter']['parameter'] or '0'
            else:
                context['limit'] = settings.COST_LIMIT
        except:
            context['limit'] = settings.COST_LIMIT
        context['month'] = str(self.month)
        context['date'] = ';'.join([str(self.month), str(self.year)])
        context['filials'] = Branch.objects.all()
        context['overlimit'] = Bills.objects.filter(Q(date__month=str(self.month)) & Q(date__year=str(self.year)) & Q(cost__gt=settings.COST_LIMIT)).count()
        return context


class EnableFilterView(View):

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        self.filter_type = kwargs['filter_type']
        self.filter_parameter = kwargs['filter_parameter']
        return  super(EnableFilterView, self).dispatch(request, *args, **kwargs)

    def get(self, request, **kwargs):
        request.session['filter'] = {}
        request.session['filter']['type'] = self.filter_type
        request.session['filter']['parameter'] = self.filter_parameter
        year, month = '', ''
        if request.META.get('HTTP_REFERER', False) and 'cards_list' in request.META['HTTP_REFERER']:
            month, year = request.META['HTTP_REFERER'].split('/')[-3:-1]
        else:
            year, month = '2012', '01'
        request.session.modified = True
        return HttpResponseRedirect(reverse('cards-list', args=[1, month, year]))


class ExportXLS(ListView):

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        self.year = kwargs['year']
        self.month = kwargs['month']
        if not request.META.get('HTTP_REFERER', False):
            request.session['filter'] = {}
            request.session['filter']['type'] = 'none'
        return  super(ExportXLS, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        if not self.request.session.get('filter', False) or self.request.session['filter']['type'] == 'none':
            queryset = Card.objects.extra(select={'cost': "select accounting_bills.cost from accounting_bills  where accounting_bills.date = '{0}-{1}-01' and accounting_card.number = accounting_bills.card_id".format(str(self.year), str(self.month))}).order_by('branch__name')
            return  [x for x in queryset if x.cost]
        if self.request.session['filter']['type'] == 'order':
            if self.request.session['filter']['parameter'] == 'minmax':
                queryset = Card.objects.extra(select={'cost': "select accounting_bills.cost from accounting_bills  where accounting_bills.date = '{0}-{1}-01' and accounting_card.number = accounting_bills.card_id".format(str(self.year), str(self.month))}).order_by('cost')
                return  [x for x in queryset if x.cost]
            else:
                queryset = Card.objects.extra(select={'cost': "select accounting_bills.cost from accounting_bills  where accounting_bills.date = '{0}-{1}-01' and accounting_card.number = accounting_bills.card_id".format(str(self.year), str(self.month))}).order_by('-cost')
                return  [x for x in queryset if x.cost]
        if self.request.session['filter']['type'] == 'filial':
            branch = Branch.objects.filter(name__startswith=self.
                                                            request.session['filter']['parameter'])
            return ([x for x in branch[0].card_set.all()] if len(branch) else [])

        if self.request.session['filter']['type'] == 'limit':
            bills = Bills.objects.filter(Q(date__month=str(self.month)) & Q(date__year=str(self.year)) & Q(cost__gt=int(self.request.session['filter']['parameter']))).prefetch_related('card')
            return [x.card for x in bills]

        if self.request.session['filter']['type'] == 'state':
            return  Card.objects.filter(state__exact='F')

    def importXLS(self):
        wb = xlwt.Workbook()
        ws = wb.add_sheet('Sheet 1')

        ws.write(0, 0, u'Номер карточки')
        ws.write(0, 1, u'Имя')
        ws.write(0, 2, u'Адрес')
        ws.write(0, 3, u'Номер концентратора')
        ws.write(0, 4, u'Номер договора')
        ws.write(0, 5, u'Дата договора')
        ws.write(0, 6, u'С кем заключен')
        ws.write(0, 7, u'Счет')

        cards = self.get_queryset()
        for i in range(len(cards)):
            ws.write(i + 1, 0, cards[i].number)
            if len(cards[i].object_set.all()):
                obj = cards[i].object_set.all()[0]
                ws.write(i + 1, 1, obj.name)
                ws.write(i + 1, 2, obj.address)
                ws.write(i + 1, 3, obj.hub.number if obj.hub else '')
                ws.write(i + 1, 4, obj.contract.number if obj.contract else '')
                ws.write(i + 1, 5, obj.contract.date if obj.contract else '')
                ws.write(
                    i + 1, 6, obj.contract.contracted if obj.contract else '')
            ws.write(i + 1, 7, cards[i].get_bill_by_month(self.month, self.year)[0] if len(
                cards[i].get_bill_by_month(self.month, self.year)) else '')
        wb.save(path.join(settings.MEDIA_ROOT, self.filename))

    def get(self, request, **kwargs):
        self.filename = 'cards_{0}.xls'.format(datetime.now())
        self.importXLS()
        return HttpResponseRedirect(path.join(settings.MEDIA_URL, self.filename))


class UploadIncomeData(FormView):
    form_class = UploadFileForm
    success_url = '/'
    template_name = 'uploadincomeform.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return  super(UploadIncomeData, self).dispatch(request, *args, **kwargs)

    @commit_on_success
    def form_valid(self, form):
        in_file = self.request.FILES['file']
        filename = in_file.name
        from parsers import parser_come
        parser = parser_come.Parser()
        filepath = self.request.FILES['file'].temporary_file_path()
        data = parser(filepath)
        for item in data:
            try:
                hub = Hub.objects.get_or_create(pk=item[3])[0]
                hub.save()
                _contract, created = Contract.objects.get_or_create(
                    number=item[4],
                    hub=hub,
                    date=item[5],
                    contracted=item[6]
                )
                if created:
                    _contract.save()
                _object = Object(
                    hub=hub,
                    name=item[0],
                    address=item[1],
                    contract=_contract
                )
                _object.save()
                _incomeObject = IncomeObject(
                    object=_object
                )
                _incomeObject.save()
            except:
                pass
        data = [{u'name': filename, u'success': True}]
        response = JSONResponse(data, {}, response_mimetype(self.request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response


class UploadOutcomeData(FormView):
    form_class = UploadFileForm
    success_url = '/'
    template_name = 'uploadoutcomeform.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return  super(UploadOutcomeData, self).dispatch(request, *args, **kwargs)

    @commit_on_success
    def form_valid(self, form):
        in_file = self.request.FILES['file']
        filename = in_file.name
        from parsers import parser_gone
        parser = parser_gone.Parser()
        filepath = self.request.FILES['file'].temporary_file_path()
        data = parser(filepath)
        for item in data:
            try:
                hub = Hub.objects.get_or_create(pk=item[3])[0]
                hub.save()
                _contract, created = Contract.objects.get_or_create(
                    number=item[4],
                    hub=hub,
                    date=item[5],
                    contracted=item[6]
                )
                if created:
                    _contract.save()
                _object, created = Object.objects.get_or_create(
                    hub=hub,
                    name=item[0],
                    address=item[1],
                    contract=_contract
                )
                _object.save()
                _outcomeObject = OutcomeObject(
                    object=_object
                )
                _outcomeObject.save()
            except:
                pass
        data = [{u'name': filename, u'success': True}]
        response = JSONResponse(data, {}, response_mimetype(self.request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response
