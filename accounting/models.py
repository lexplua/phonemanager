# -*- coding: utf-8 -*-
from django.db import models
from django.db.models import Q
from django.db.models.signals import pre_save
from django.dispatch import receiver
from datetime import datetime


class Branch(models.Model):
    name = models.CharField(max_length=50, verbose_name=u'Название филиала', primary_key=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Филиал'
        verbose_name_plural = u'Филиалы'


class Card(models.Model):
    number = models.CharField(max_length=12, verbose_name=u'Номер карты', primary_key=True)
    state = models.CharField(max_length=1, choices=(
            ('R', 'Резерв'),
            ('W', 'Работает'),
            ('F', 'Свободна'),
        ), verbose_name=u'Состояние карты'
    )
    new_state = models.CharField(max_length=1, choices=(
            ('R', 'Резерв'),
            ('W', 'Работает'),
            ('F', 'Свободна'),
        ), verbose_name=u'Новое состояние карты'
    )
    branch = models.ForeignKey(Branch, verbose_name=u'Филиал', blank=True, null=True)
    P_accepted = models.BooleanField(default=False, verbose_name=u'Новое состояние подтверджено пультом')
    OD_accepted = models.BooleanField(default=False, verbose_name=u'Новое состояние подтверджено отделом договоров')
    B_accepted = models.BooleanField(default=False, verbose_name=u'Новое состояние подтверджено бухгалтерией')

    def get_bill_by_month(self, month, year):
        return [x.cost for x in self.bills_set.filter(Q(date__month=month) & Q(date__year=year))]

    def __unicode__(self):
        return self.number

    def get_absolute_url(self):
        return "/admin/accounting/card/{0}/".format(self.number)

    class Meta:
        verbose_name = u'Карта'
        verbose_name_plural = u'Карты'


class Hub(models.Model):
    number = models.CharField(max_length=50, verbose_name=u'Номер концентратора', primary_key=True)

    def __unicode__(self):
        return self.number

    class Meta:
        verbose_name = u'Концентратор'
        verbose_name_plural = u'Концентраторы'


class Contract(models.Model):
    number = models.CharField(max_length=12, verbose_name=u'Номер договора', primary_key=True)
    date = models.DateField(verbose_name=u'Дата заключения')
    contracted = models.CharField(max_length=120, verbose_name=u'С кем заключен')
    hub = models.ForeignKey(Hub, verbose_name=u'Концентратор')

    def __unicode__(self):
        return self.number

    class Meta:
        verbose_name = u'Договор'
        verbose_name_plural = u'Договора'


class Object(models.Model):
    hub = models.ForeignKey(Hub, verbose_name=u'Концентратор', blank=True, null=True)
    name = models.CharField(max_length=50, verbose_name=u'Название обьекта')
    cards = models.ManyToManyField(Card, verbose_name=u'Карта', blank=True, null=True)
    branch = models.ForeignKey(Branch, verbose_name=u'Филиал', blank=True, null=True)
    address = models.CharField(max_length=120, verbose_name=u'Адрес обьекта')
    contract = models.ForeignKey(Contract, verbose_name=u'Договор', blank=True, null=True)
    not_alive = models.BooleanField(default=False, verbose_name=u"Обьект удален")
    delete_date = models.DateField(blank=True, null=True, default=None, verbose_name=u"Дата удаления")

    def __unicode__(self):
        return u"{0} , {1}".format(self.name, self.address)

    class Meta:
        ordering=['-delete_date']
        verbose_name = u'Обьект'
        verbose_name_plural = u'Обьекты'


class Bills(models.Model):
    card = models.ForeignKey(Card, verbose_name=u'Карта')
    date = models.DateField(verbose_name=u'Дата счета')
    cost = models.FloatField(verbose_name=u'Сумма счета')
    ordering = ('card',)

    def __unicode__(self):
        return u"{0} : {1}".format(self.card, self.date)

    class Meta:
        verbose_name = u'Счет'
        verbose_name_plural = u'Счета'


class IncomeObject(models.Model):
    object = models.ForeignKey(Object)

    class Meta:
        verbose_name = u'Взятый'
        verbose_name_plural = u'Взятые'


class OutcomeObject(models.Model):
    object = models.ForeignKey(Object)

    class Meta:
        verbose_name = u'Ушедший'
        verbose_name_plural = u'Ушедшие'


@receiver(pre_save, sender=Card)
def inspect_status_card(instance, **kwargs):
    if isinstance(instance, Card):
        try:
            old_instance = Card.objects.get(pk=instance.pk)
            if instance.state != instance.new_state and instance.new_state != old_instance.new_state:
                instance.B_accepted = False
                instance.OD_accepted = False
                instance.P_accepted = False

            elif instance.state != instance.new_state and reduce(lambda x, q: x and q, [instance.B_accepted, instance.OD_accepted, instance.P_accepted]):
                instance.state = instance.new_state
        except:
            pass
