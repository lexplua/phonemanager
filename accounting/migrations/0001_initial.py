# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Branch'
        db.create_table('phonemanager_branch', (
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50, primary_key=True)),
        ))
        db.send_create_signal('accounting', ['Branch'])

        # Adding model 'Card'
        db.create_table('phonemanager_card', (
            ('number', self.gf('django.db.models.fields.CharField')(max_length=12, primary_key=True)),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('new_state', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('branch', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['accounting.Branch'], null=True, blank=True)),
            ('P_accepted', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('OD_accepted', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('B_accepted', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('accounting', ['Card'])

        # Adding model 'Hub'
        db.create_table('phonemanager_hub', (
            ('number', self.gf('django.db.models.fields.CharField')(max_length=50, primary_key=True)),
        ))
        db.send_create_signal('accounting', ['Hub'])

        # Adding model 'Contract'
        db.create_table('phonemanager_contract', (
            ('number', self.gf('django.db.models.fields.CharField')(max_length=12, primary_key=True)),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('contracted', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('hub', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['accounting.Hub'])),
        ))
        db.send_create_signal('accounting', ['Contract'])

        # Adding model 'Object'
        db.create_table('phonemanager_object', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('hub', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['accounting.Hub'], null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('card', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['accounting.Card'], null=True, blank=True)),
            ('branch', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['accounting.Branch'], null=True, blank=True)),
            ('contract', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['accounting.Contract'], null=True, blank=True)),
        ))
        db.send_create_signal('accounting', ['Object'])

        # Adding model 'Bills'
        db.create_table('phonemanager_bills', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('card', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['accounting.Card'])),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('cost', self.gf('django.db.models.fields.FloatField')()),
        ))
        db.send_create_signal('accounting', ['Bills'])

        # Adding model 'IncomeObject'
        db.create_table('phonemanager_incomeobject', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('object', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['accounting.Object'])),
        ))
        db.send_create_signal('accounting', ['IncomeObject'])

        # Adding model 'OutcomeObject'
        db.create_table('phonemanager_outcomeobject', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('object', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['accounting.Object'])),
        ))
        db.send_create_signal('accounting', ['OutcomeObject'])


    def backwards(self, orm):
        # Deleting model 'Branch'
        db.delete_table('phonemanager_branch')

        # Deleting model 'Card'
        db.delete_table('phonemanager_card')

        # Deleting model 'Hub'
        db.delete_table('phonemanager_hub')

        # Deleting model 'Contract'
        db.delete_table('phonemanager_contract')

        # Deleting model 'Object'
        db.delete_table('phonemanager_object')

        # Deleting model 'Bills'
        db.delete_table('phonemanager_bills')

        # Deleting model 'IncomeObject'
        db.delete_table('phonemanager_incomeobject')

        # Deleting model 'OutcomeObject'
        db.delete_table('phonemanager_outcomeobject')


    models = {
        'accounting.bills': {
            'Meta': {'object_name': 'Bills'},
            'card': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['accounting.Card']"}),
            'cost': ('django.db.models.fields.FloatField', [], {}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'accounting.branch': {
            'Meta': {'object_name': 'Branch'},
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'primary_key': 'True'})
        },
        'accounting.card': {
            'B_accepted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'Meta': {'object_name': 'Card'},
            'OD_accepted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'P_accepted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'branch': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['accounting.Branch']", 'null': 'True', 'blank': 'True'}),
            'new_state': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'number': ('django.db.models.fields.CharField', [], {'max_length': '12', 'primary_key': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        'accounting.contract': {
            'Meta': {'object_name': 'Contract'},
            'contracted': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'hub': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['accounting.Hub']"}),
            'number': ('django.db.models.fields.CharField', [], {'max_length': '12', 'primary_key': 'True'})
        },
        'accounting.hub': {
            'Meta': {'object_name': 'Hub'},
            'number': ('django.db.models.fields.CharField', [], {'max_length': '50', 'primary_key': 'True'})
        },
        'accounting.incomeobject': {
            'Meta': {'object_name': 'IncomeObject'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['accounting.Object']"})
        },
        'accounting.object': {
            'Meta': {'object_name': 'Object'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'branch': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['accounting.Branch']", 'null': 'True', 'blank': 'True'}),
            'card': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['accounting.Card']", 'null': 'True', 'blank': 'True'}),
            'contract': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['accounting.Contract']", 'null': 'True', 'blank': 'True'}),
            'hub': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['accounting.Hub']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'accounting.outcomeobject': {
            'Meta': {'object_name': 'OutcomeObject'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['accounting.Object']"})
        }
    }

    complete_apps = ['accounting']