__author__ = 'lex'
from django import forms


class UploadFileForm(forms.Form):
    file = forms.FileField(required=True)